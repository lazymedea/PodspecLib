# PodspecLib

[![CI Status](https://img.shields.io/travis/lazymedea/PodspecLib.svg?style=flat)](https://travis-ci.org/lazymedea/PodspecLib)
[![Version](https://img.shields.io/cocoapods/v/PodspecLib.svg?style=flat)](https://cocoapods.org/pods/PodspecLib)
[![License](https://img.shields.io/cocoapods/l/PodspecLib.svg?style=flat)](https://cocoapods.org/pods/PodspecLib)
[![Platform](https://img.shields.io/cocoapods/p/PodspecLib.svg?style=flat)](https://cocoapods.org/pods/PodspecLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PodspecLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PodspecLib'
```

## Author

lazymedea, lazymedea@gmail.com

## License

PodspecLib is available under the MIT license. See the LICENSE file for more info.
