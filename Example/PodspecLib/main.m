//
//  main.m
//  PodspecLib
//
//  Created by lazymedea on 05/04/2024.
//  Copyright (c) 2024 lazymedea. All rights reserved.
//

@import UIKit;
#import "MKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MKAppDelegate class]));
    }
}
